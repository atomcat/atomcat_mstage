
atomcat_preprocess_os_lrc4 <- function(df) {
      
    #Completed treatment: complete-case analysis
    df <- df[!is.na(df$compl_treatement),]
    df <- df[df$compl_treatement != 'NA',]
    
    #Radiotherapy technique: converting to IMRT/VMAT vs 3D conformal
    df <- df[!is.na(df$rt_technique),]
    for (i in 1:nrow(df)) {
      if (df$rt_technique[i] <= 1) {
        df$rt_technique[i] <- 0
      } else if (df$rt_technique[i] > 1) {
        df$rt_technique[i] <- 1
      }
    }
    
    #Age: converting to cutoff: 50-69 vs <50 and 70+ vs <50
    df$age50to69 <- NA
    df$age70plus <- NA
    for (i in 1:nrow(df)) {
      if (df$age[i] >= 50 & df$age[i] <= 69) {
        df$age50to69[i] <- 1
      } else {
        df$age50to69[i] <- 0
      } 
    }
    
    for (i in 1:nrow(df)) {
      if (df$age[i] >= 70) {
        df$age70plus[i] <- 1
      } else {
        df$age70plus[i] <- 0
      } 
    }
    
    #Age: converting to age squared
    df$agesquared <- NA
    for (i in 1:nrow(df)) {
      df$agesquared[i] <- df$age[i]^2
    } 
    
    #Chemotherapy regimen: create dummy variables
    df$chemo_cat1 <- NA
    df$chemo_cat2 <- NA
    df$chemo_cat3 <- NA
    for (i in 1:nrow(df)) {
      if (df$chemo_cat[i] == 1) {
        df$chemo_cat1[i] <- 1
      } else {
        df$chemo_cat1[i] <- 0
      } 
    }
    
    for (i in 1:nrow(df)) {
      if (df$chemo_cat[i] == 2) {
        df$chemo_cat2[i] <- 1
      } else {
        df$chemo_cat2[i] <- 0
      } 
    }
    
    for (i in 1:nrow(df)) {
      if (df$chemo_cat[i] == 3) {
        df$chemo_cat3[i] <- 1
      } else {
        df$chemo_cat3[i] <- 0
      } 
    }
    
    #Categorical GTV: create dummy variables
    df$gtv_cat1 <- NA
    df$gtv_cat2 <- NA
    df$gtv_cat3 <- NA
    df$gtv_cat4 <- NA
    for (i in 1:nrow(df)) {
      if (df$gtv_cat[i] == 1) {
        df$gtv_cat1[i] <- 1
      } else {
        df$gtv_cat1[i] <- 0
      } 
    }
    
    for (i in 1:nrow(df)) {
      if (df$gtv_cat[i] == 2) {
        df$gtv_cat2[i] <- 1
      } else {
        df$gtv_cat2[i] <- 0
      } 
    }
    
    for (i in 1:nrow(df)) {
      if (df$gtv_cat[i] == 3) {
        df$gtv_cat3[i] <- 1
      } else {
        df$gtv_cat3[i] <- 0
      } 
    }
    
    for (i in 1:nrow(df)) {
      if (df$gtv_cat[i] == 4) {
        df$gtv_cat4[i] <- 1
      } else {
        df$gtv_cat4[i] <- 0
      } 
    }
    
   return(df)  
      
}