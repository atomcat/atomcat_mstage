# vtg.coxph
Implementation of the Cox Proportional Hazards algorithm for the Vantage6 federated infrastructure.

## Installation
Run the following in the R console to install the package and its dependencies:
```R
# This also installs the package vtg
# Clone the repository in local and select path till /src
install.packages("C:/path to folder with the package", repos = NULL, type = "source")
```

## Example use
```R
setup.client <- function() {
  # Define parameters
  username <- "username@example.com"
  password <- "password"
  host <- 'https://trolltunga.vantage6.ai'
  api_path <- ''
  
  # Create the client
  client <- vtg::Client$new(host, api_path=api_path)
  client$authenticate(username, password)

  return(client)
}

# Create a client
client <- setup.client()

# Get a list of available collaborations
print( client$getCollaborations() )

# Should output something like this:
#   id     name
# 1  1 ZEPPELIN
# 2  2 PIPELINE

# Select a collaboration
client$setCollaborationId(1)
client$setUseMasterContainer(F)
client$set.task.image("ananyac/vtg.coxph:atomcat_filter")

# Define explanatory variables, time column and censor column
expl_vars <- c("age","sex","t_stage")
time_col <- "dm_fup"
censor_col <- "dm_status"

# vtg.coxph contains the function `dcoxph`.
######################values for filter data. Each value will select a different filter##################
###################### Values beyond [1,8] will run the algorithm without any filters applied ###########
  # 1 : DM1
  # 2 : DM2
  # 3 : DM3
  # 4 : OS_LRC1
  # 5 : OS_LRC2
  # 6 : OS_LRC3
  # 7 : OS_LRC4
  # 8 : OS_LRC5

result <- vtg.coxph::dcoxph(client, expl_vars, time_col, censor_col, filter_data=c(1))
```

## Example use for testing
```R
# Load a dataset
df <- read.csv("path_to_dataset")

# Define explanatory variables, time column and censor column
expl_vars <- c("age","sex","t_stage")
time_col <- "dm_fup"
censor_col <- "dm_status"

######################values for filter data. Each value will select a different filter##################
###################### Values beyond [1,8] will run the algorithm without any filters applied ###########
  # 1 : DM1
  # 2 : DM2
  # 3 : DM3
  # 4 : OS_LRC1
  # 5 : OS_LRC2
  # 6 : OS_LRC3
  # 7 : OS_LRC4
  # 8 : OS_LRC5
result <- vtg.coxph::dcoxph.mock(df, expl_vars, time_col, censor_col,filter_data=c(1))
```
